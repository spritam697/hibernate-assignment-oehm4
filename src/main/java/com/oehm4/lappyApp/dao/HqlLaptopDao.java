package com.oehm4.lappyApp.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.oehm4.lappyApp.dto.Laptop;

public class HqlLaptopDao {

	public List<Laptop> getAllLappyHql() {
		Configuration configuration = new Configuration();
		configuration.configure();
		Session session = configuration.buildSessionFactory().openSession();
		String hql = "from Laptop";// dto class name;
		Query query = session.createQuery(hql);
		List<Laptop> list = query.list();

		return list;
	}
	public void updatePriceByIdHql(Integer lid,Double price)
	{
		Configuration configuration = new Configuration();
		configuration.configure();
		Session session = configuration.buildSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		String hql="update Laptop set price=:price where lid=:lid";
		Query query = session.createQuery(hql);
		query.setParameter("price", price);
		query.setParameter("lid", lid);
		int updateRows = query.executeUpdate();
		transaction.commit();
		if(updateRows == 0) {
			System.out.println("Update Operation Failed");
		}
		System.out.println("Update Operation successfull");
	}
	public void deleteById(Integer lid) {
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		String hql="delete Laptop where lid=:lid";
		Query query = session.createQuery(hql);
		query.setParameter("lid",lid);
		int updateRows = query.executeUpdate();
		transaction.commit();
		if(updateRows == 0) {
			System.out.println("Delete Operation Failed");
		}
		System.out.println("Delete Operation successfull");
	}

}
