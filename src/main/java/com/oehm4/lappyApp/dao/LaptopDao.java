package com.oehm4.lappyApp.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.oehm4.lappyApp.dto.Laptop;

public class LaptopDao {

	public void saveLaptop(Laptop laptop) {
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");
		//configuration.addAnnotatedClass(Laptop.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		// Session
		// session=configuration.configure().buildSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.save(laptop);
		transaction.commit();
	}

	public Laptop getOneLaptop(Integer id) {
		Configuration configuration = new Configuration();
		configuration.configure();
		//configuration.addAnnotatedClass(Laptop.class);
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		return (Laptop) session.get(Laptop.class, id);
		// return (Laptop)session.load(Laptop.class,id);
	}
	public List<Laptop> getAllLaptop()
	{
		Configuration configuration = new Configuration();
		configuration.configure();
		Session session = configuration.buildSessionFactory().openSession();
		
		return null;
	}

	public void updateLaptop(Integer id, Double price) {
		Laptop oneLaptop = getOneLaptop(id);
		if (oneLaptop != null) {
			Configuration configuration = new Configuration();
			configuration.configure();
			//configuration.addAnnotatedClass(Laptop.class);
			Session session = configuration.buildSessionFactory().openSession();
			Transaction transaction = session.beginTransaction();
			oneLaptop.setPrice(price);
			session.update(oneLaptop);
			System.out.println("price updated");
			transaction.commit();
		}
		else
			System.out.println("price updation failed");

	}
}
