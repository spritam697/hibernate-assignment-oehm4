package com.oehm4.lappyApp.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="laptop_tab")
public class Laptop implements Serializable{
	@Id
	@GenericGenerator(name="lid",strategy="increment")
	@GeneratedValue(generator="lid")
	@Column(name="lid")
	private Integer lid;
	@Column(name="brand")
	private String brand;
	@Column(name="model")
	private String model;
	@Column(name="price")
	private Double price;
	
	public Integer getLid() {
		return lid;
	}
	public void setLid(Integer lid) {
		this.lid = lid;
	}
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Laptop [lid=" + lid + ", brand=" + brand + ", model=" + model + ", price=" + price + "]";
	}
	public Laptop() {
		super();
	}
	
	
	
	
	
}
