package com.oehm4.lappyApp.util;

import com.oehm4.lappyApp.dao.HqlLaptopDao;
import com.oehm4.lappyApp.dao.LaptopDao;
import com.oehm4.lappyApp.dto.Laptop;

public class LaptopTest {

	public static void main(String[] args) {
		/*
		Laptop laptop = new Laptop();
		laptop.setBrand("HP");
		laptop.setModel("Pavilion 15");
		laptop.setPrice(42500.00);
		
		Laptop laptop1 = new Laptop();
		laptop1.setBrand("HP");
		laptop1.setModel("Pavilion 16");
		laptop1.setPrice(546500.00);
		
		Laptop laptop2 = new Laptop();
		laptop2.setBrand("HP");
		laptop2.setModel("Pavilion 17");
		laptop2.setPrice(3254300.00);
		
		Laptop laptop3 = new Laptop();
		laptop3.setBrand("HP");
		laptop3.setModel("Pavilion 19");
		laptop3.setPrice(325320.00);
		
		LaptopDao dao = new LaptopDao();
		dao.saveLaptop(laptop);
		dao.saveLaptop(laptop1);
		dao.saveLaptop(laptop2);
		dao.saveLaptop(laptop3);
		*/
		
		//PROCESS ONE
		/*
		LaptopDao dao2 = new LaptopDao();
		Laptop l1 = dao2.getOneLaptop(2);
		if(l1!=null)
		{
			System.out.println(l1);
		}
		else
			System.out.println("laptop details with the given id is not found");
		
		Laptop l2 = dao2.getOneLaptop(2);
		if(l1!=null)
		{
			System.out.println(l2);
		}
		else
			System.out.println("laptop details with the given id is not found");
		
		//PROCESS TWO
		//LaptopDao dao = new LaptopDao();
		//System.out.println(dao.getOneLaptop(2));
		*/
		
		/*
		LaptopDao dao = new LaptopDao();
		dao.updateLaptop(2,10000.00);
		*/
		
		HqlLaptopDao hqlLaptopDao = new HqlLaptopDao();
		//hqlLaptopDao.updatePriceByIdHql(2,10000.00);
		hqlLaptopDao.deleteById(5);
		System.out.println(hqlLaptopDao.getAllLappyHql());
		
	}
}
